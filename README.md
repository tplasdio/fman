<p align="center">
  <h1 align="center">fman</h1>
  <p align="center">
    Interactive manual and info selector with fzf, rofi or dmenu.
  </p>
  <img
    width="1200"
    src="https://codeberg.org/tplasdio/fman/raw/branch/main/media/ss1.avif"
    alt="image not loaded"
  />
</p>

## 📰 Description

Open a selectable list of manuals and infodocs and open
the document with `man` or `info`, your editor or as a PDF.

## 🚀 Installation

<details>
<summary>📦 From source</summary>

- *Dependencies*:
  - `man`
  - `info`
  - `fzf`
  - [`getoptions`](https://github.com/ko1nksm/getoptions)
- *Optional Dependencies*:
  - `gettext` for translations
  - `rofi`
  - `dmenu`
  - `zathura` or a PDF reader
  - One of: `ghostscript`, `vim`, `nvim`, `pandoc` or libreoffice (for PDF with info)

```sh
git clone --filter=blob:none https://codeberg.org/tplasdio/fman.git
install -Dm755 fman /usr/local/bin
```

</details>

<details>
<summary>Arch Linux or other pacman distros</summary>

```sh
curl -O https://codeberg.org/tplasdio/fman/raw/branch/main/packaging/PKGBUILD-git
makepkg -sip PKGBUILD-git
```

For uninstallation, run `sudo pacman -Rsn fman-git`.

</details>

## 📟 Usage

```
Select manuals/infos interactively with fzf

Usage:
  fman [-m] [-i] [-e | -p] [-r | -d] [SEARCH_TERMS]

Options:
  -m, --man                   Only manuals (no infos)
  -i, --info                  Only infos
  -e, --editor                Open with $EDITOR
  -p, --pdf                   Open as PDF with $READER
  -r, --rofi                  Select with rofi
  -d, --dmenu                 Select with dmenu
      --color                 Show fzf manual previews with colors
```

## 💭 Similar

- [`macho`](https://hiphish.github.io/blog/2020/05/31/macho-man-command-on-steroids/)
- [`sman`](https://github.com/jtojnar/sman)

## 📝 License

BSD-3-clause
